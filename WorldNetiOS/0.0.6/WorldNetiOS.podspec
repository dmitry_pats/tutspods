Pod::Spec.new do |s|
  s.name          = "WorldNetiOS"
  s.version       = "0.0.6"
  s.summary       = "World Net text."
  s.homepage      = "https://bitbucket.org/DmitryVironIT/worldnetios"
  s.license       = "MIT"
  s.license       = { :type => "MIT", :file => "LICENSE" }
  s.author        = { "jurre" => "d.pats@vironit.com" }
  s.platform      = :ios
  s.ios.deployment_target = '9.0'
  s.source        = { :git => "https://DmitryVironIT@bitbucket.org/DmitryVironIT/worldnetios.git", :tag => "0.0.6" }
  s.source_files  = "WorldNetiOS", "WorldNetiOS/**/*.{h,m}"
  s.frameworks    = "UIKit"
  s.requires_arc  = true
  s.vendored_library = 'WorldNetiOS/libs/libwni.a'
end
