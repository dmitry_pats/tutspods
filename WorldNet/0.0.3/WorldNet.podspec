Pod::Spec.new do |s|
  s.name          = "WorldNet"
  s.version       = "0.0.3"
  s.summary       = "World Net text."
  s.homepage      = "https://bitbucket.org/DevAntalika/offlinetranslateframework_ios"
  s.license       = "MIT"
  s.license       = { :type => "MIT", :file => "LICENSE" }
  s.author        = { "jurre" => "d.pats@vironit.com" }
  s.platform      = :osx
  s.osx.deployment_target = '10.8'
  s.source        = { :git => "https://DmitryVironIT@bitbucket.org/DmitryVironIT/worldnet.git", :tag => "0.0.3" }
  s.source_files  = "WorldNet", "WorldNet/**/*.{h,m}"
  s.frameworks    = "SystemConfiguration"
  s.requires_arc  = true
  s.vendored_library = 'WorldNet/libs/libwni.a'
end
